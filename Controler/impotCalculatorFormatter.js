const ImpotCalculator = require('../Model/ImpotCalculator/impotCalculator')

class ImpotCalculatorControleur {
  constructor (listParticulier, listTaux) {
    this.Particuliers = listParticulier
    this.Taux = listTaux
    this.impotCalculator = new ImpotCalculator(this.Taux, this.Particuliers)
  }

  /**
   * Ajout a une liste les donnees formetter pour chaque Particulier.
   * @returns {[{impotProvincial: string, moyenneProvincial: string, nom: String, impotFederal: string, moyenneFederal: string}]} Liste de donne formatter
   */
  getFormatedDataForAllParticulier () {
    const resulta = []
    const impotCalculator = this.impotCalculator
    this.Particuliers.forEach(function (particulier) {
      impotCalculator.setParticulier(particulier)
      resulta.push(formatParticulier(impotCalculator))
    })
    return resulta
  }

  /**
   * Retourne les donnees formatter pour le Particulier qui ce situe a l'index.
   * @param {number} index
   * @returns {{impotProvincial: string, moyenneProvincial: string, nom: String, impotFederal: string, moyenneFederal: string}}
   */
  getFormatedDataParticulier (index) {
    this.impotCalculator.setParticulier(this.Particuliers[index])
    return formatParticulier(this.impotCalculator)
  }
}

/**
 * Format les donnes du particulier en memoire dans l'impot calculator.
 * @param impotCalculator
 * @returns {{impotProvincial: string, moyenneProvincial: string, nom: String, impotFederal: string, moyenneFederal: string}}
 */
function formatParticulier (impotCalculator = this.ImpotCalulator) {
  return {
    nom: impotCalculator.getNomParticulier(),
    impotFederal: impotCalculator.calculerFederal() + '$',
    moyenneFederal: impotCalculator.calculerTauxMoyenFederal() + '%',
    impotProvincial: impotCalculator.calculerProvincial() + '$',
    moyenneProvincial: impotCalculator.calculerTauxMoyenProvincial() + '%'
  }
}

module.exports = ImpotCalculatorControleur
