const ImpotCalculatorBase = require('./impotCalculatorBase')

/**
 * La class ImpotCalculator utilise ImpotCalculator Base.
 * Cree un ImpotCalculteur pour les deux paliers de gouvernement.
 */
class ImpotCalulator {
  /**
   * Constructeur du calculateur d'impot.
   * @param taux - List de taux
   * @param particulier - Objet Particulier
   */
  constructor (taux, particulier) {
    this.federalImpotCalculator =
      new ImpotCalculatorBase(taux.federal.montantPersonnelBase, taux.federal.paliers, particulier)
    this.provincialImpotCalculator =
      new ImpotCalculatorBase(taux.provincial.montantPersonnelBase, taux.provincial.paliers, particulier)
    this.nom = particulier.nom
  }

  /**
   *
   * @param particulier
   */
  setParticulier (particulier) {
    this.federalImpotCalculator.setParticulier(particulier)
    this.provincialImpotCalculator.setParticulier(particulier)
    this.nom = particulier.nom
  }

  /**
   * Calcul le montant d'impostion du au niveau Federal pour le particulier en memoire.
   * @returns {Number} Impot Federal
   */
  calculerFederal () {
    return this.federalImpotCalculator.calculateParticulier()
  }

  /**
   * Calcul le taux d'imposition moyen au niveau Federal pour le particulier en memoire.
   * @returns {Number}
   */
  calculerTauxMoyenFederal () {
    return this.federalImpotCalculator.calcultateTauxMoyen()
  }

  /**
   * Calcul le montant d'impostion du au niveau Provincial pour le particulier en memoire.
   * @returns {Number} Impot Provincial.
   */
  calculerProvincial () {
    return this.provincialImpotCalculator.calculateParticulier()
  }

  /**
   * Calcul le taux d'imposition moyen au niveau Provincial pour le particulier en memoire.
   * @returns {Number} Taux moyen.
   */
  calculerTauxMoyenProvincial () {
    return this.provincialImpotCalculator.calcultateTauxMoyen()
  }

  /**
   * Retourne le nom particulier en memoire.
   * @returns {String} Nom
   */
  getNomParticulier () {
    return this.nom
  }
}

module.exports = ImpotCalulator
