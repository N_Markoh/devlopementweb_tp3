
class ImpotCalculatorBase {
  /**
   * Caculateur d'impot base.
   * @param montantPersonnelBase
   * @param paliers
   * @param particulier
   */
  constructor (montantPersonnelBase, paliers = [], particulier) {
    this.montantPersonnelBase = montantPersonnelBase
    this.paliers = paliers
    this.particulier = particulier
  }

  /**
   * calcul le montant du au impot pour un Particulier
   * @returns {int} impot
   */
  calculateParticulier () {
    let impot = 0
    let reste
    let revenu = this.particulier.revenu
    const montantPersonnelBase = this.montantPersonnelBase
    this.paliers.forEach(function (palier) {
      if (palier.montant <= revenu) {
        reste = revenu - palier.montant
        if (palier.montant === 0) {
          reste -= montantPersonnelBase
        }
        impot += reste * palier.taux
        revenu = palier.montant
      }
    })

    return impot.toFixed(2)
  }

  /**
   * Cacul le taux moyen d'un particulier
   * @returns {int} Taux d'imposition moyen pour un particulier.
   */
  calcultateTauxMoyen () {
    return (this.calculateParticulier() / this.particulier.revenu * 100).toFixed(2)
  }

  setParticulier (particulier) {
    this.particulier = particulier
  }
}
module.exports = ImpotCalculatorBase
