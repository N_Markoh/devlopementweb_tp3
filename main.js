const ImpotCalculatorFormatter = require('./Controler/impotCalculatorFormatter')

/**
 * Prend un liste de particulier en argument
 *
 * @var liste de particulier.
 * @return une array de particulier objets.
 **/
function injestParticuliers (listParticulier = []) {
  const result = []
  listParticulier.forEach(function (particulier) {
    result.push(new Particulier(particulier.nom, particulier.revenus))
  })
  return result
}

/**
 * Objet Particulier
 *
 * @var string nom
 * @var revenu int
 *
 **/
class Particulier {
  constructor (nom, revenu) {
    this.nom = nom
    this.revenu = revenu
  }
}

/**
 * Hardcoded entree enfin de tester.
 * @type {string}
 */
const entree = '[\n' +
  '    {\n' +
  '        "particuliers":[\n' +
  '           {\n' +
  '             "nom": "Alice X",\n' +
  '             "revenus": "60000"\n' +
  '           },\n' +
  '           {\n' +
  '             "nom": "Bob Yota",\n' +
  '             "revenus": "10000"\n' +
  '           },\n' +
  '           {\n' +
  '             "nom": "Charlie Zeta",\n' +
  '             "revenus": "1000000"\n' +
  '           }\n' +
  '       ]\n' +
  '   },\n' +
  '   {\n' +
  '      "taux": {\n' +
  '        "federal": {\n' +
  '          "montantPersonnelBase": 13808,\n' +
  '          "paliers": [\n' +
  '            {\n' +
  '              "montant": 0,\n' +
  '              "taux": 0.15\n' +
  '            },\n' +
  '            {\n' +
  '              "montant": 49020,\n' +
  '              "taux": 0.205\n' +
  '            },\n' +
  '            {\n' +
  '              "montant": 98040,\n' +
  '              "taux": 0.26\n' +
  '            },\n' +
  '            {\n' +
  '              "montant": 151978,\n' +
  '              "taux": 0.29\n' +
  '            },\n' +
  '            {\n' +
  '              "montant": 216511,\n' +
  '              "taux": 0.33\n' +
  '            }\n' +
  '          ]\n' +
  '        },\n' +
  '        "provincial": {\n' +
  '          "montantPersonnelBase": 15728,\n' +
  '          "paliers": [\n' +
  '            {\n' +
  '              "montant": 0,\n' +
  '              "taux": 0.15\n' +
  '            },\n' +
  '            {\n' +
  '              "montant": 45105,\n' +
  '              "taux": 0.2\n' +
  '            },\n' +
  '            {\n' +
  '              "montant": 90200,\n' +
  '              "taux": 0.24\n' +
  '            },\n' +
  '            {\n' +
  '              "montant": 109755,\n' +
  '              "taux": 0.2575\n' +
  '            }\n' +
  '          ]\n' +
  '        }\n' +
  '      }\n' +
  '   }\n' +
  ']'

const parsedEntree = JSON.parse(entree)
const listParticulier = injestParticuliers(parsedEntree[0].particuliers)
const listTaux = parsedEntree[1].taux

const impotCalculatorFormatter = new ImpotCalculatorFormatter(listParticulier, listTaux)

console.log(impotCalculatorFormatter.getFormatedDataForAllParticulier())
console.log(impotCalculatorFormatter.getFormatedDataParticulier(0))
